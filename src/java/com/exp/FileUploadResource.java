/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp;

import com.db.DataBasePlugin;
import com.db.MessagePropertiesUtil;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author APTOL301655
 */
@Path("/multiupload")
public class FileUploadResource {
    int count = 0;
    @Path("/claassrooms/{UserName}/{roomid}/{classid}/{version}")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String uploadFiles2(
            @FormDataParam("files") FormDataBodyPart bodyParts,
            @PathParam("UserName") String UserName,
            @PathParam("roomid") String roomid,
            @PathParam("classid") String classid,
            @PathParam("version") String deviceVersion) throws JSONException {
        String directory = "";
        String result = "";
        String Appversion = "3";
        ArrayList<String> list=new ArrayList<String>();

        JSONObject jobj = new JSONObject();
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

//        directory = "\\\\10.3.3.206\\E$\\ISMS\\UDISE\\" + UserName + "\\CLASSROOMS\\" + roomid + "\\" + classid + "\\";

        try {
            if (deviceVersion != null & Appversion.equals(deviceVersion)) {
                if (classid.equalsIgnoreCase("Not Applicable")) {
                      con = DataBasePlugin.getConnectionUDISE();
                    cstmt = con.prepareCall("{call USP_Datacapture_classrooms_latest(?,?,?,?,?,?,?,?,?,?,?)}");
                    cstmt.setString(1, UserName);
                    cstmt.setString(2, roomid);
                    cstmt.setString(3, classid);
                    cstmt.setString(4, "");
                    cstmt.setString(5, "");
                    cstmt.setString(6, "");
                    cstmt.setString(7, "");
                    cstmt.setString(8, "");
                    cstmt.setString(9, "");
                    cstmt.setString(10, "");
                    cstmt.setString(11, "");
                    rs = cstmt.executeQuery();
                    while (rs.next()) {
                        result = rs.getString(1);
                    }
                    if (result.equalsIgnoreCase("1")) {
                        jobj.put("data", "");
                        jobj.put("status", "1");
                        jobj.put("msg", "Status updated Successfully");
                    } else {
                        jobj.put("data", "");
                        jobj.put("status", "0");
                        jobj.put("msg", "Status updation Failed");
                    }
                } else {
                    directory = MessagePropertiesUtil.FILE_PATH + UserName + "\\CLASSROOMS\\" + roomid + "\\";
                    if (directory != null && !"".equals(directory) && directory.length() > 0) { // If directory is not exists it will create
                        File directorytemp = new File(directory);
                        if (!directorytemp.exists()) {
                            directorytemp.mkdirs();
                        }
                    }
                    for (BodyPart part : bodyParts.getParent().getBodyParts()) {
                        InputStream is = part.getEntityAs(InputStream.class);
                        ContentDisposition filedet = part.getContentDisposition();
                        String location = directory + filedet.getFileName();
                        list.add(location);
                        saveFile(is, location);
                    }
                    if (count == 8) {
                        con = DataBasePlugin.getConnectionUDISE();
                        cstmt = con.prepareCall("{call USP_Datacapture_classrooms_latest(?,?,?,?,?,?,?,?,?,?,?)}");
                        cstmt.setString(1, UserName);
                        cstmt.setString(2, roomid);
                        cstmt.setString(3, classid);
                        cstmt.setString(4, list.get(0));
                        cstmt.setString(5, list.get(1));
                        cstmt.setString(6, list.get(2));
                        cstmt.setString(7, list.get(3));
                        cstmt.setString(8, list.get(4));
                        cstmt.setString(9, list.get(5));
                        cstmt.setString(10, list.get(6));
                        cstmt.setString(11, list.get(7));
                        rs = cstmt.executeQuery();
                        while (rs.next()) {
                            result = rs.getString(1);
                        }
                        if (result.equalsIgnoreCase("1")) {
                            jobj.put("data", "");
                            jobj.put("status", "1");
                            jobj.put("msg", "Images uploaded Successfully");
                        } else {
                            jobj.put("data", "");
                            jobj.put("status", "0");
                            jobj.put("msg", "Images upload Failed");
                        }
            }else{
                        jobj.put("data", "");
                        jobj.put("status", "0");
                        jobj.put("msg", "Images upload Failed");
                    }
                }
            } else {
                jobj.put("msg", "Updated Version available, Check for Updates");
                jobj.put("status", "2");
                jobj.put("data", "");
                jobj.put("url", "");
            }
        } catch (Exception e) {
            jobj.put("data", "");
            jobj.put("status", "0");
            jobj.put("msg", e.getMessage());
        }
        return jobj.toString();

    }

    private void saveFile(InputStream is, String name) {
        try {
            final File uploadingFileInTempFolder = new File(name);
            FileUtils.copyInputStreamToFile(is, uploadingFileInTempFolder);
            count++;
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}