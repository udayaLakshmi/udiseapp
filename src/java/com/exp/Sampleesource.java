/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp;

import com.db.DataBasePlugin;
import com.db.MessagePropertiesUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author APTOL301655
 */
@Path("/filesupload")
public class Sampleesource {

    int count = 0;

    @POST
    @Path("/upload/{UserName}/{version}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail, @PathParam("UserName") String UserName,
            @PathParam("version") String deviceVersion) throws JSONException, SQLException {
        String directory = "";
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String Appversion = "3";
        JSONObject jobj = new JSONObject();
        try {
            if (deviceVersion != null & Appversion.equals(deviceVersion)) {
                directory = MessagePropertiesUtil.FILE_PATH + UserName + "\\CAMPUS\\";
                if (directory != null && !"".equals(directory) && directory.length() > 0) { // If directory is not exists it will create
                    File directorytemp = new File(directory);
                    if (!directorytemp.exists()) {
                        directorytemp.mkdirs();
                    }
                }
                String uploadedFileLocation = directory + fileDetail.getFileName();
                writeToFile(uploadedInputStream, uploadedFileLocation);
                if (count == 1) {
                    jobj.put("data", "");
                    jobj.put("status", "1");
                    jobj.put("msg", "Video uploaded Successfully");
                } else {
                    jobj.put("data", "");
                    jobj.put("status", "0");
                    jobj.put("msg", "Video uploadFailed");
                }
            } else {
                  jobj.put("msg", "Updated Version available, Check for Updates");
                jobj.put("status", "2");
                jobj.put("data", "");
                 jobj.put("url", "");
            }
        } catch (Exception e) {
             jobj.put("data", "");
            jobj.put("status", "0");
            jobj.put("msg", e.getMessage());
        }
        return jobj.toString();

    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) throws JSONException {

        try {
            final File uploadingFileInTempFolder = new File(uploadedFileLocation);
            FileUtils.copyInputStreamToFile(uploadedInputStream, uploadingFileInTempFolder);
            count = 1;
        } catch (IOException e) {

            count = 0;
            e.printStackTrace();
        }

    }
}
