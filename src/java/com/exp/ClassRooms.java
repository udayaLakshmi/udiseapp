/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp;

import com.db.DataBasePlugin;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author APTOL301655
 */
@Path("/classrooms")
public class ClassRooms {
int count=0;
@POST
    @Path("/upload/{UserName}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
        @FormDataParam("file") FormDataContentDisposition fileDetail,
        @PathParam("UserName") String UserName
        ,@PathParam("classid") String classid
        ) throws JSONException, SQLException {
    String directory="";  
    Connection con=null;
        PreparedStatement pstmt=null;
        ResultSet rs=null;
           directory = "\\\\10.3.37.204\\E$\\ISMS\\UDISE\\" + UserName + "\\CLASSROOMS\\"+classid+"\\";
//           directory = "\\\\10.3.3.206\\E$\\ISMS\\UDISE\\" + UserName + "\\CLASSROOMS\\";
//        directory = "D:\\ISMS\\UDISE\\" + UserName + "\\CLASSROOMS\\"+classid+"\\";   
        if (directory != null && !"".equals(directory) && directory.length() > 0) { // If directory is not exists it will create
                    File directorytemp = new File(directory);
                    if (!directorytemp.exists()) {
                        directorytemp.mkdirs();
                    }
                }
         String uploadedFileLocation =directory+fileDetail.getFileName();
        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);

         JSONObject jobj=new JSONObject();
        if(count==1){
//            try{
//            con = DataBasePlugin.getConnectionUDISE();
//            pstmt=con.prepareStatement("update Tbl_DataCapture_PaintingSchoolCampus set Image3=? where SchoolCode=? ");
//            pstmt.setString(1,fileDetail.getFileName());
//            pstmt.setString(2,UserName);
//             count=pstmt.executeUpdate();
//            }catch(Exception e){
//                e.printStackTrace();
//            }
        jobj.put("data","");
        jobj.put("status","1");
        jobj.put("msg","Images uploaded Successfully");
        }else{
        jobj.put("data","");
        jobj.put("status","0");
        jobj.put("msg","Images upload Failed"); 
        }
        return jobj.toString();

    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream,
        String uploadedFileLocation) throws JSONException {
 
      try {
          final File uploadingFileInTempFolder = new File(uploadedFileLocation);
           FileUtils.copyInputStreamToFile(uploadedInputStream, uploadingFileInTempFolder);
            count=1;
        } catch (IOException e) {

        count=0;
        e.printStackTrace();
        }

    }    
}
